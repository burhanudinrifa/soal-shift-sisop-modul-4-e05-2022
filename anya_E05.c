#define FUSE_USE_VERSION 28
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fuse.h>
#include <dirent.h>

static  const  char *dirpath = "/home/[user]/Documents";
static  const  char *dirpathAnime = "/home/[user]/Documents/Animeku_";
static struct fuse_operations xmp_oper={
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}


char *rot13(const char *filename)
{
    if(src == NULL){
      return NULL;
    }
  
    char* result = malloc(strlen(src));
    
    if(result != NULL){
      strcpy(result, src);
      char* current_char = result;
      
      while(*current_char != '\0'){
        //Only increment alphabet characters
        if((*current_char >= 97 && *current_char <= 122) || (*current_char >= 65 && *current_char <= 90)){
          if(*current_char > 109 || (*current_char > 77 && *current_char < 91)){
            //Characters that wrap around to the start of the alphabet
            *current_char -= 13;
          }else{
            //Characters that can be safely incremented
            *current_char += 13;
          }
        }
        current_char++;
      }
    }
    return result;
}

char* atbashCipher(char* filename){
    char normal[26]={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
    char cipher[26]={'z','y','x','w','v','u','t','s','r','q','p','o','n','m','l','k','j','i','h','g','f','e','d','c','b','a'};
    char final[256];
    int buffer;
    int size=sizeof(filename)/sizeof(filename[0]);
    for(int j=0; j<size; j++){
        for(int i=0; i<25; i++){
            if(filename[j]==normal[i]){
                buffer=i;
            }
        }
        final[j]=cipher[i]
    }
}

// void split(char* filename){
//     char delim='/';
//     char* splitted;
//     splitted=strtok(filename,delim);//the first substring
//     while(splitted!=null){
//         splitted=strtok(NULL, delim);//the next substring
//     }
// }

// bool isDirAnime(){
//     DIR *d;
//     struct dirent *dir;
//     bool isAnime=false;
//     d = opendir(dirpath);
//     if (d)
//     {
//         while ((dir = readdir(d)) != NULL)
//         {
//             //printf("%s\n", dir->d_name);
//             if(strstr(dir->d_name,"Animeku_")){
//                 isAnime=true;
//             }
//         }
//         closedir(d);
//         return isAnime;
//     }
// }

bool isStringAllCapOnaStackJustFaxNoPrinter(char* filename){
int size=sizeof(filename)/sizeof(filename[0]);
bool final=true;
for(int j=0; j<size;j++){
    if(isupper(filename[j])==0){
        final=false;
    }
}
return final;
}

static int do_rename(const char *before, const char *after){
    if(strcmp(after,"Animeku_")==0){
    DIR *d;
    char* ciphered;
    struct dirent *dir;
    d=opendir(dirpathAnime);
    if(d){
        while((dir=readdir(d))!=NULL){
            if(isStringAllCapOnaStackJustFaxNoPrinter(dir->d_name)==true){
               ciphered=atbashChiper(dir->d_name);
            }
            else{
                ciphered=rot13(dir->d_name);
            }
            rename(dir->d_name,chipered);
        }
    }
    }
}
int main(){
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}